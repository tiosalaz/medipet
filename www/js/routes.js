angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  
      .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'MainCtrl'
  })
      .state('app.tabsController.inicio', {
    url: '/inicio',
    views: {
      'tab2': {
        templateUrl: 'templates/inicio.html',
        controller: 'inicioCtrl'
      }
    }
  })
    .state('app.tabsController.misCitas', {
    url: '/mis-citas',
    views: {
      'tab1': {
        templateUrl: 'templates/misCitas.html',
        controller: 'misCitasCtrl'
      }
    }
  })
  .state('app.pedirCita', {
    url: '/pedir-cita',
    templateUrl: 'templates/pedirCita.html',
    controller: 'pedirCitaCtrl'
  })

  .state('app.tabsController.pedirCita2', {
    url: '/cita-ind/:serId',
    views: {
      'tab3': {
        templateUrl: 'templates/pedirCita2.html',
        controller: 'pedirCita2Ctrl'
      }
    }
  })


  .state('app.tabsController.misMascotas', {
    url: '/mismascotas',
    views: {
      'tab3': {
        templateUrl: 'templates/misMascotas.html',
        controller: 'misMascotasCtrl'
      }
    }
  })

  .state('app.tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('iniciarSesiN', {
    url: '/login',
    templateUrl: 'templates/iniciarSesiN.html',
    controller: 'iniciarSesiNCtrl'
  })

  .state('app.tabsController.perfil', {
    url: '/perfil',
    views: {
      'tab3': {
        templateUrl: 'templates/perfil.html',
        controller: 'perfilCtrl'
      }
    }
  })

  .state('app.tabsController.historiaClinica', {
    url: '/historia-clinica',
    views: {
      'tab3': {
        templateUrl: 'templates/historiaClinica.html',
        controller: 'historiaClinicaCtrl'
      }
    }
  })

  .state('app.tabsController.servicios', {
    url: '/servicios',
    views: {
      'tab3': {
        templateUrl: 'templates/servicios.html',
        controller: 'serviciosCtrl'
      }
    }
  })

$urlRouterProvider.otherwise('/login')

  

});