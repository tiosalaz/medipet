// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic', 'app.controllers', 'app.routes', 'app.services', 'app.directives'])

.run(function($ionicPlatform,$state, $ionicPopup) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
   

       setTimeout(function() {
        navigator.splashscreen.hide();
    }, 300);
                         
    // Función Botón atrás
       /* $ionicPlatform.registerBackButtonAction(function () {
            var confirmPopup = $ionicPopup.confirm({
                 title: 'Salir',
                 template: 'Deseas salir de la Aplicación?'
               });
                  confirmPopup.then(function(res) {
                 if(res) {
                   localStorage.removeItem('usuario');
                     localStorage.removeItem('pets');
                     localStorage.removeItem('pet');
                ionic.Platform.exitApp();
                 } 
               });  

        }, 100);*/

        /*if (localStorage.getItem('usuario')){  

                 $state.go('app.tabsController.inicio'); 
        }else{
           $state.go('iniciarSesiN'); 
        }*/

        $ionicPlatform.on('pause', function() {
      localStorage.setItem('usuario', JSON.stringify($rootScope.usuario || {}));

      // opcionalmente puedes guardar la pagina donde se encuentra el usuario
      localStorage.setItem('hash', document.location.hash);
    });

    $ionicPlatform.on('resume', function() {
       var usuario = localStorage.getItem('usuario');
       $rootScope.usuario = JSON.parse(usuario);

       // restauras el estado cuando fue cerrada la app.
       document.location.hash = localStorage.getItem('hash');
    });  

  });
})
.config(function ($httpProvider) {
  $httpProvider.defaults.headers.common = {};
  $httpProvider.defaults.headers.post = {};
  $httpProvider.defaults.headers.put = {};
  $httpProvider.defaults.headers.patch = {};
});
